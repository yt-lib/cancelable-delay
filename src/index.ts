import type { AbortSignal } from 'abort-controller'

export class DOMException extends Error {
	constructor(message: string, name: string) {
		super(message)
		this.name = name
	}
}

type Opts = {
	signal?: AbortSignal
}

/**
 * @throws {DOMException & { name: 'AbortError' }}
 */
export const cancelableDelay = (timeout: number, opts?: Opts) =>
	new Promise<void>((resolve, reject) => {
		const signal = opts?.signal
		const id = setTimeout(() => {
			signal?.removeEventListener('abort', onAbort)
			resolve()
		}, timeout)
		const onAbort = () => {
			clearTimeout(id)
			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			signal!.removeEventListener('abort', onAbort)
			reject(new DOMException('aborted', 'AbortError'))
		}
		signal?.addEventListener('abort', onAbort)
	})

export { Opts as CancelableDelayOptions }
