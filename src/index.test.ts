import { AbortController } from 'abort-controller'
import { cancelableDelay, DOMException } from '.'

describe('DOMException', () => {
	const x = new DOMException('hoge', 'fuga')
	test('message', () => {
		expect(x.message).toBe('hoge')
	})

	test('name', () => {
		expect(x.name).toBe('fuga')
	})

	test('is error', () => {
		expect(x).toBeInstanceOf(Error)
	})
})

describe('cancelableDelay', () => {
	test('no error', async () => {
		await cancelableDelay(10)
	})

	test('no error 2', async () => {
		const ctl = new AbortController()
		await cancelableDelay(10, { signal: ctl.signal })
	})

	test('aborted', async () => {
		const ctl = new AbortController()
		setTimeout(() => ctl.abort(), 1)
		await expect(cancelableDelay(300, { signal: ctl.signal })).rejects.toEqual(
			new DOMException('aborted', 'AbortError'),
		)
	})
})
